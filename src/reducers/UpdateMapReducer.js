
const INITIAL_STATE = {
    data: 'null',   
};

export default (state = INITIAL_STATE, action) =>{
    switch(action.type){
    case 'BUS_UPDATE':
        return { ...state, 'data': action.payload};
       
    default:
        return state;
    }
   
}