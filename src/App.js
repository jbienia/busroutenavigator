import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import {Provider} from 'react-redux';

import {connect} from 'react-redux';
import MapLogic from './Components/MapLogic';

class App extends Component {

   

   makeStore(){
   return createStore(reducers);
  
   }

  render() {
    return (
      <Provider store = {this.makeStore()}>
        <div className="App">
         
          
          
          <MapLogic />
          
        </div>
      </Provider>
    );
  }
}



export default App;
