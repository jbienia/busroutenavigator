import React , {Component} from 'react';
import {connect} from 'react-redux';
import {updateBusLocation} from '../actions';
import axios from 'axios';
import ReactMapGL, {Marker} from 'react-map-gl';
import FaBeer from 'react-icons/lib/fa/beer';
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import asyncPoll from 'react-async-poll';

var longitude;
var latitude;
var proxy = require('express-http-proxy');

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiamJpZW5pYSIsImEiOiJjamFlbjhvamUwd2NiMnhsZXR2ZnFmeDE2In0.x0psCnD4HnKAS34tDb3CRw"
  });

class MapLogic extends Component{

    componentWillMount(){  
        axios.get('http://localhost:8080/buses',{
            responseType: 'json',     
        })
        
        .then(response => this.props.updateBusLocation(response.data));   
           
            this.longitude = -123.116226;
            this.latitude = 49.246292;
         }

    componentDidMount(){
      setInterval(this.tick.bind(this), 3000);  
    }

    tick(){
        axios.get('http://localhost:8080/buses',{
            responseType: 'json',
        })
        .then(response => this.props.updateBusLocation(response.data))      
    }

    displayMarkers(){
        if( this.props.data !== 'null'){          
            return  this.props.data.map(value => <Feature key={value.TripId} coordinates={[value.Longitude,value.Latitude]}/>);         
        }     
    }
    
    storeLatitudeAndLongitude(longAndLat){     
        const {lng,lat} = longAndLat;
        this.longitude = lng;
        this.latitude = lat;      
    }

    render(){
       
        return(
            <div>
                    <Map
                        style="mapbox://styles/jbienia/cjaeotgu76e442sp6ifzjk0id"
                        center={[this.longitude, this.latitude]}
                        containerStyle={{
                            height: "100vh",
                            width: "100vw"
                        }}
                                          
                        onStyleLoad={(viewport)=>{
                        }}
                        onDrag={(viewport)=>{this.storeLatitudeAndLongitude(viewport.transform._center)}}                                       
                        >

                        <Layer
                        type="symbol"
                        id="marker"
                        layout={{ "icon-image": "marker-15" }}>
                        {this.displayMarkers()}
                        </Layer>
                    </Map>
          </div>
        );
    };
};

const mapStateToProps = (state) =>{
   
    const {data} = state.mapData;
  
    return {data}
  };

export default connect(mapStateToProps,{updateBusLocation})(MapLogic);