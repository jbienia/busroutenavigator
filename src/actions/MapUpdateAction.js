
export const updateBusLocation = (data) =>{
    return{
        type: 'BUS_UPDATE',
        payload: data
    };
};