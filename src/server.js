
const express = require('express');
var request = require('request');
var rp = require('request-promise');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));

app.get('/buses', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
   
   var options = {
    uri: 'http://api.translink.ca/rttiapi/v1/buses?apikey=SS2kKrsTGpNqHZ35LKvL',
   
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true 
};

rp(options)
    .then(function (repos) {
        return res.json(repos);
     
    })
    .catch(function (err) {
        // API call failed...
    });
});


app.listen(process.env.PORT || 8080);