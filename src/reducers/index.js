import {combineReducers} from 'redux';
import UpdateMapReducer from './UpdateMapReducer';

export default combineReducers({
    mapData: UpdateMapReducer
});